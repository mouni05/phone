import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  name:any;
  number:any;
  address: any;
  array: any=[];
  data: any=[];

  save(){
    let newdata={
    name: this.name,
    number:this.number,
    address:this.address
    };

    this.data.push(newdata);
    this.data.sort((a:any,b:any) => a.name.localeCompare(b.name));
    this.name="";
    this.number="";
    this.address="";
  }


  constructor() { }

  ngOnInit(): void {
  }

}
